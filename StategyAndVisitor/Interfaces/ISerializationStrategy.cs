﻿using System;

namespace StategyAndVisitor
{
    /// <summary>
    /// Стратегия сериализации фигуры.
    /// </summary>
    internal interface ISerializationStrategy
    {

        /// <summary>
        /// Метод сериализует объект.
        /// </summary>
        /// <typeparam name="T">Тип фигуры.</typeparam>
        /// <param name="figure">Фигура.</param>
        /// <param name="visitor">Посетитель.</param>
        /// <returns></returns>
        string Serialize<T>(T figure, IVisitor visitor);
    }
}
