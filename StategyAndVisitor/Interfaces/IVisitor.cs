﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StategyAndVisitor
{
    /// <summary>
    /// Посетитель.
    /// </summary>
    interface IVisitor
    {
        string Execute<T>(T figure, XmlSerializationStrategy strategy);
        string Execute<T>(T figure, JSONSerializationStrategy strategy);
    }
}
