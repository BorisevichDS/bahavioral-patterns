﻿using System;

namespace StategyAndVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DotDemo();
                CircleDemo();
                SquareDemo();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

        private static void DotDemo()
        {
            Console.WriteLine("Dot demo");
            Console.WriteLine();

            ISerializationStrategy strategy = new XmlSerializationStrategy();
            SerializationContext context = new SerializationContext(strategy);
            Dot figure = new Dot(4, 5);
            IVisitor visitor = new DotVisitor();

            string result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();

            context.SetStrategy(new JSONSerializationStrategy());
            result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();
        }

        private static void CircleDemo()
        {
            Console.WriteLine("Circle demo");
            Console.WriteLine();

            ISerializationStrategy strategy = new XmlSerializationStrategy();
            SerializationContext context = new SerializationContext(strategy);
            Circle figure = new Circle(5);
            IVisitor visitor = new CircleVisitor();

            string result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();

            context.SetStrategy(new JSONSerializationStrategy());
            result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();
        }

        private static void SquareDemo()
        {
            Console.WriteLine("Square demo");
            Console.WriteLine();

            ISerializationStrategy strategy = new XmlSerializationStrategy();
            SerializationContext context = new SerializationContext(strategy);
            Square figure = new Square(10);
            IVisitor visitor = new SquareVisitor();

            string result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();

            context.SetStrategy(new JSONSerializationStrategy());
            result = context.Serialize(figure, visitor);
            Console.WriteLine(result);
            Console.WriteLine();
        }
    }
}
