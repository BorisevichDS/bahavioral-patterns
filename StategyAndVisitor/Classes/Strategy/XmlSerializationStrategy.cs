﻿using System;

namespace StategyAndVisitor
{
    /// <summary>
    /// Стратегия сериализации объектов в XML.
    /// </summary>
    internal class XmlSerializationStrategy : ISerializationStrategy
    {
        /// <summary>
        /// Метод сериализует объект.
        /// </summary>
        /// <typeparam name="T">Тип фигуры.</typeparam>
        /// <param name="figure">Фигура, которую необходимо сериализовать.</param>
        /// <param name="visitor">Посетитель.</param>
        /// <returns></returns>
        public string Serialize<T>(T figure, IVisitor visitor)
        {
            return visitor.Execute(figure, this);
        }
    }
}
