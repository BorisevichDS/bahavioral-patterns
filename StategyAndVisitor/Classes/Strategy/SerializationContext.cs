﻿using System;

namespace StategyAndVisitor
{
    /// <summary>
    /// Контекст для работы со стратегиями.
    /// </summary>
    internal class SerializationContext
    {
        private ISerializationStrategy strategy;

        public SerializationContext(ISerializationStrategy strategy)
        {
            this.strategy = strategy;
        }

        /// <summary>
        /// Задает стратегию.
        /// </summary>
        /// <param name="strategy"></param>
        public void SetStrategy(ISerializationStrategy strategy) => this.strategy = strategy;

        /// <summary>
        /// Метод сериализует объект.
        /// </summary>
        /// <typeparam name="T">Тип фигуры.</typeparam>
        /// <param name="figure">Фигура.</param>
        /// <param name="visitor">Посетитель.</param>
        /// <returns></returns>
        public string Serialize<T>(T figure, IVisitor visitor) => strategy.Serialize(figure, visitor);
    }
}
