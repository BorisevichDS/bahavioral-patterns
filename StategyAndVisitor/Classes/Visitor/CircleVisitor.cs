﻿using System;
using System.Text;

namespace StategyAndVisitor
{
    internal class CircleVisitor : IVisitor
    {
        public string Execute<T>(T figure, XmlSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Circle)
            {
                var f = figure as Circle;

                sb.Append($"<figure type=\"{f.GetType().Name}\">");
                sb.Append(Environment.NewLine);
                sb.Append($"<Radius>{f.Radius}</Radius>");
                sb.Append(Environment.NewLine);
                sb.Append($"<Area>{f.Area}</Area>");
                sb.Append(Environment.NewLine);
                sb.Append("</figure>");
            }
            else
            {
                sb.Append("Error! Figure is not circle.");
            }
            return sb.ToString();
        }

        public string Execute<T>(T figure, JSONSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Circle)
            {
                var f = figure as Circle;

                sb.Append("{");
                sb.Append(Environment.NewLine);
                sb.Append($"\"type\": \"{f.GetType().Name}\",");
                sb.Append(Environment.NewLine);
                sb.Append($"\"Radius\": {f.Radius},");
                sb.Append(Environment.NewLine);
                sb.Append($"\"Area\": {f.Area}");
                sb.Append(Environment.NewLine);
                sb.Append("}");
            }
            else
            {
                sb.Append("Error! Figure is not circle.");
            }
            return sb.ToString();
        }
    }
}
