﻿using System;
using System.Text;

namespace StategyAndVisitor
{
    internal class DotVisitor : IVisitor
    {
        public string Execute<T>(T figure, XmlSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Dot)
            {
                var f = figure as Dot;

                sb.Append($"<figure type=\"{f.GetType().Name}\">");
                sb.Append(Environment.NewLine);
                sb.Append($"<X>{f.X}</X>");
                sb.Append(Environment.NewLine);
                sb.Append($"<Y>{f.Y}</Y>");
                sb.Append("</figure>");
            }
            else
            {
                sb.Append("Error! Figure is not dot.");
            }
            return sb.ToString();
        }

        public string Execute<T>(T figure, JSONSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Dot)
            {
                var f = figure as Dot;

                sb.Append("{");
                sb.Append(Environment.NewLine);
                sb.Append($"\"type\": \"{f.GetType().Name}\",");
                sb.Append(Environment.NewLine);
                sb.Append($"\"X\": {f.X},");
                sb.Append(Environment.NewLine);
                sb.Append($"\"Y\": {f.Y}");
                sb.Append(Environment.NewLine);
                sb.Append("}");
            }
            else
            {
                sb.Append("Error! Figure is not dot.");
            }
            return sb.ToString();
        }
    }
}
