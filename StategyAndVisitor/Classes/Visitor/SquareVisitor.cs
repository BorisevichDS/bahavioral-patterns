﻿using System;
using System.Text;

namespace StategyAndVisitor
{
    internal class SquareVisitor : IVisitor
    {
        public string Execute<T>(T figure, XmlSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Square)
            {
                var f = figure as Square;

                sb.Append($"<figure type=\"{f.GetType().Name}\">");
                sb.Append(Environment.NewLine);
                sb.Append($"<SideLength>{f.SideLength}</SideLength>");
                sb.Append(Environment.NewLine);
                sb.Append($"<Perimeter>{f.Perimeter}</Perimeter>");
                sb.Append(Environment.NewLine);
                sb.Append($"<Area>{f.Area}</Area>");
                sb.Append(Environment.NewLine);
                sb.Append("</figure>");
            }
            else
            {
                sb.Append("Error! Figure is not square.");
            }
            return sb.ToString();
        }

        public string Execute<T>(T figure, JSONSerializationStrategy strategy)
        {
            var sb = new StringBuilder();

            if (figure is Square)
            {
                var f = figure as Square;

                sb.Append("{");
                sb.Append(Environment.NewLine);
                sb.Append($"\"type\": \"{f.GetType().Name}\",");
                sb.Append(Environment.NewLine);
                sb.Append($"\"SideLength\": {f.SideLength},");
                sb.Append(Environment.NewLine);
                sb.Append($"\"Perimeter\": {f.Perimeter},");
                sb.Append(Environment.NewLine);
                sb.Append($"\"Area\": {f.Area}");
                sb.Append(Environment.NewLine);
                sb.Append("}");
            }
            else
            {
                sb.Append("Error! Figure is not square.");
            }
            return sb.ToString();
        }
    }
}
