﻿using System;

namespace StategyAndVisitor
{
    /// <summary>
    /// Круг.
    /// </summary>
    [Serializable]
    internal class Circle //: IFigure
    {
        /// <summary>
        /// Радиус.
        /// </summary>
        
        public double Radius { get; }

        /// <summary>
        /// Площадь.
        /// </summary>
        
        public double Area => Math.PI * Radius * Radius;

        public Circle(double radius)
        {
            Radius = radius;
        }
    }
}
