﻿using System;

namespace StategyAndVisitor
{
    /// <summary>
    /// Точка.
    /// </summary>
    [Serializable]
    internal class Dot //: IFigure
    {
        /// <summary>
        /// Координата X.
        /// </summary>
        public int X { get; }

        /// <summary>
        /// Координата Y.
        /// </summary>
        public int Y { get; }

        public Dot(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
