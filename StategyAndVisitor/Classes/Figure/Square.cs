﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StategyAndVisitor
{
    /// <summary>
    /// Квадрат.
    /// </summary>
    [Serializable]
    internal class Square //: IFigure
    {
        /// <summary>
        /// Длина стороны.
        /// </summary>
        public double SideLength { get; }

        /// <summary>
        /// Периметер.
        /// </summary>
        public double Perimeter => 4 * SideLength;

        /// <summary>
        /// Площадь.
        /// </summary>
        public double Area => SideLength * SideLength;

        public Square(double sideLength)
        {
            SideLength = sideLength;
        }
    }
}
